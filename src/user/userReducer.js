import Actions from "../actions/actions";

const initialState = {
  data: null,
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case Actions.USER_LOGGED_IN:
      return {
        ...state,
        data: action.payload
      };
    case Actions.USER_LOGGED_OUT:
      return {
        ...state,
        data: null
      };
    default:
      return state;
  }
};

export default userReducer;
