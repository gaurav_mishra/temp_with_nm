import React, { Component } from "react";
import { Link } from "react-router";
import { HiddenOnlyAuth, VisibleOnlyAuth } from "./util/wrappers.js";

// UI Components
import LoginButtonContainer from "./user/ui/loginbutton/LoginButtonContainer";
import LogoutButtonContainer from "./user/ui/logoutbutton/LogoutButtonContainer";
import SideDrawer from "./components/SideDrawer";
import { hideSideBar } from "./actions/actionDispatcher";
import store from "./store.js";

// Styles
import "./css/oswald.css";
import "./css/open-sans.css";
import "./css/pure-min.css";
import "./App.css";
import "../public/css/main.module.css";
import { connect } from "react-redux";
import lolLogo from "../public/assets/lol_logo.svg";
import Footer from "./layouts/footer/Footer";

const mapStateToProps = state => {
  return {
    data: state.app
  };
};

const mapDispatchToProps = () => {
  return {};
};

class App extends Component {
  closeSideDrawer = () => {
    store.dispatch(hideSideBar());
  };



  render() {
    const OnlyAuthLinks = VisibleOnlyAuth(() => (
      <span>
        <li className="pure-menu-item">
          {/* <Link to="/dashboard" className="pure-menu-link">
            Dashboard
          </Link> */}
        </li>
        {/* <li className="pure-menu-item">
          <Link to="/profile" className="pure-menu-link">
            Profile
          </Link>
        </li>
        <li className="pure-menu-item">
          <Link to="/about" className="pure-menu-link">
            About
          </Link>
        </li>
        <li className="pure-menu-item">
          <Link to="/contact" className="pure-menu-link">
            Contact
          </Link>
        </li> */}
        <LogoutButtonContainer />
      </span>
    ));

    const OnlyGuestLinks = HiddenOnlyAuth(() => (
      <span>
        {/* <li className="pure-menu-item">
          <Link to="/about" className="pure-menu-link">
            About
          </Link>
        </li>
        <li className="pure-menu-item">
          <Link to="/contact" className="pure-menu-link">
            Contact
          </Link>
        </li> */}
        <LoginButtonContainer />
      </span>
    ));

    return (
      <div className="App">
        <div className="app_bg">
          <div className="app_wrapper_overlay">
            <div className="app_linear_gradient">
              <nav className="navbar pure-menu pure-menu-horizontal nav_custom">
                <Link
                  to="/"
                  className="pure-menu-heading pure-menu-link logo_wrapper"
                >
                  <img className="lol_logo" src={lolLogo} alt="logo" />
                </Link>
                <ul className="pure-menu-list navbar-right nc__menu">
                  <OnlyGuestLinks />
                  <OnlyAuthLinks />
                </ul>
              </nav>
              <SideDrawer
                oncloseSideDrawerClick={this.closeSideDrawer}
                showFlag={this.props.data.showSideBar}
              />
              {this.props.children}
              <Footer />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
