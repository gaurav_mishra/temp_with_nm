import Actions from "./actions";

export function showSideBar() {
  return {
    type: Actions.SHOW_SIDEBAR
  };
}

export function hideSideBar() {
  return {
    type: Actions.HIDE_SIDEBAR
  };
}

export function showLoaderOnConnectFormSubmit(){
  return{
    type: Actions.SHOW_LOADER_ON_CONNECT_FORM_SUBMIT
  }
}


export function addPlayerID (payload){
  return{
    type: Actions.ADD_PLAYER_ID,
    payload: payload
  }
}


export function addUserDetails(payload){
  return{
    type: Actions.SAVE_USER_DATA,
    payload: payload
  }
}
