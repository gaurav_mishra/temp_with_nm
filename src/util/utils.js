export const setLocalStorage = (keyName, value) => {
  localStorage.setItem(keyName, value);
};

export const getLocalStorage = keyName => {
  return localStorage.getItem(keyName);
};

export const timeConvert = n => {
  var num = n;
  var hours = num / 60;
  var rhours = Math.floor(hours);
  var minutes = (hours - rhours) * 60;
  var rminutes = Math.round(minutes);
  return `${rhours} : ${rminutes} MINS`;
};

let current_datetime = new Date();
export const formattedDate =
  current_datetime.getDate() +
  "/" +
  (current_datetime.getMonth() + 1) +
  "/" +
  current_datetime.getFullYear();
