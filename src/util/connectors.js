import { Connect, SimpleSigner } from 'uport-connect'
export let uport = new Connect('Blockchain - VoG', {
  clientId: '2oqE3JeLFq1oRTAasfdEhEpYQiLjKmgG4kn',
  network: 'rinkeby',
  signer: SimpleSigner('e19bd825c9673b536bff50866b81efdef393db7eaedbdd675834958935c36b31')
})
export const web3 = uport.getWeb3()