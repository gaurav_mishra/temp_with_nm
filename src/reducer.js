import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import userReducer from './user/userReducer'
import appReducer from './reducers/appReducer'

const reducer = combineReducers({
  routing: routerReducer,
  user: userReducer,
  app: appReducer,
})

export default reducer
