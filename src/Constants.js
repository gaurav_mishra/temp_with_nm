const Constants = {
    locationList: [
        "BR1",
        "EUN1",
        "EUW1",
        "JP1",
        "KR",
        "LA1",
        "LA2",
        "NA1",
        "OC1",
        "TR1",
        "RU",
    ]
}

export const cPlayerID = "playerID"

export const cLocation = "location"

export const cSummonerName = "summonerName"

export const defaultRegionValue = "Select"

export const baseIconsUrl = 'http://ddragon.leagueoflegends.com/cdn/10.2.1/img/item/';

export default Constants

