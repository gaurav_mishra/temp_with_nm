import React from 'react';
import { baseIconsUrl } from '../../Constants';

const ColumnBlock = (props) => {
    const noIcon = props.icons && props.icons.length === 0 ? <p>NA</p> : null
    return (
        <div className={`col_wrapper ${props.customClass && props.customClass}`}>
            <div className="user_label">{props.label}</div>
                <div className="user_value font_bebas">
                    {typeof (props.value) === "number" && props.value}
                    {typeof (props.value) === "string" && props.value}
                </div>
            {noIcon}
            {
                props.kda && props.kda ?
                    <div className="user_value font_bebas">{props.kda}</div>
                    :
                    null
            }
            {
                props.status &&
                    props.status ?
                    <div className={`user_value font_bebas ${props.status && props.status === 'win' ? 'usr_val_green' : 'usr_val_red'}`}>
                        {props.status}
                    </div>
                    :
                    null
            }
            {
                props.icons
                    ?
                    <div className="user_items">
                        {props.icons.map((item, key) => {
                            return (
                                <img key={key} src={`${baseIconsUrl.concat(item.itemId)}.png`} alt="icon" className="item" />
                            )
                        })}
                    </div>
                    :
                    null
            }
        </div >
    )
}

export default ColumnBlock