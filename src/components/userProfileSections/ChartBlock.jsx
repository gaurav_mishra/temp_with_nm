import React from 'react';
import store from '../../store';

const ChartBlock = (props) => {
    const appData = store.getState()
    let gamesPercent
    let winsPercent
    let lossesPercent
    const matchesLength = appData && appData.app.playerData && appData.app.playerData.matches ? appData.app.playerData.matches.length : null;
    // const matchesLength = 18;
    if (matchesLength) {
        const games = props.games
        const wins = props.wins
        const losses = props.losses
        gamesPercent = ((games * 100) / matchesLength).toFixed(0);
        winsPercent = ((wins * 100) / matchesLength).toFixed(0);
        lossesPercent = ((losses * 100) / matchesLength).toFixed(0);
    }
    else {
        console.log('use length 18')
    }
    const getHeightStyle = param => {
        return (
            {
                height: `calc(0% + ${param}%)`
            }
        )
    }

    return (
        <div className="chart_block">
            <div className="cb__container">
                <span className={`block b__gray ${gamesPercent <= 0 ? 'inc_margin' : null}`} style={getHeightStyle(gamesPercent)}></span>
                <span className={`block b__green ${winsPercent <= 0 ? 'inc_margin' : null}`} style={getHeightStyle(winsPercent)}></span>
                <span className={`block b__red ${lossesPercent <= 0 ? 'inc_margin' : null}`} style={getHeightStyle(lossesPercent)}></span>
            </div>
            <div>
                <img src={props.userPhoto} alt="user" className={`user_photo ${props.smallUserPhoto ? 'small_user_photo' : null}`} />
            </div>
            {
                props.rolePlayed
                    ?
                    <p className="role_name">{props.rolePlayed}</p>
                    :
                    null
            }
        </div>
    )
}


export default ChartBlock