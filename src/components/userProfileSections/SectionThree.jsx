import React from 'react';
import GameRow from './GameRow';

const SectionThree = (props) => {
    return (
        <div className="section_three">
            <div className="font_bebas sub_title">Most Recent Games</div>
            {
                props.matches ?
                props.matches.map((arr, key) => {
                    return(
                        <GameRow key={key} matchData={arr}/>
                    )
                })
                :
                <p className="no_data_msg">NA</p>
            }
        </div>
    )
}


export default SectionThree