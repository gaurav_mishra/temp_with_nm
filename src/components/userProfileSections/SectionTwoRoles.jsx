import React from 'react';
import ChartBlock from './ChartBlock';


import TOP from '../../../public/assets/user/roles/role_top.svg';
import BOTTOM from '../../../public/assets/user/roles/role_bottom.svg';
import MID from '../../../public/assets/user/roles/role_mid.svg';
import JUNGLE from '../../../public/assets/user/roles/role_jungle.svg';
import SUPPORT from '../../../public/assets/user/roles/role_support.svg';


const rolesIcons = [TOP, MID, BOTTOM, SUPPORT, JUNGLE]



const SectionTwoRoles = (props) => {
    const rolePlayed = props.roles ? Object.keys(props.roles) : null
    return (
        <div className="section_two">
            <div className="st__top_row">
                <div className="font_bebas sub_title">{props.subTitle ? props.subTitle : null}</div>
                <div className="color_block_wrapper">
                    <div className="cb_holder">
                        <span className="cb cb__gray"></span>
                        <span className="inline_block">Games</span>
                    </div>
                    <div className="cb_holder">
                        <span className="cb cb__green"></span>
                        <span className="inline_block">Wins</span>
                    </div>
                    <div className="cb_holder">
                        <span className="cb cb__red"></span>
                        <span className="inline_block">Losses</span>
                    </div>
                </div>
            </div>
            <div className="st__bottom_row">
                {
                    props.roles ?
                        Object.values(props.roles).map((arr, key) => {
                            return (
                                <ChartBlock
                                    key={key}
                                    rolePlayed={rolePlayed[key]}
                                    userPhoto={arr.icon}
                                    smallUserPhoto={true}
                                    games={arr.games}
                                    losses={arr.losses}
                                    wins={arr.wins}
                                />
                            )
                        })
                        : <p className="no_data_msg font_size_twice">NA</p>
                }
            </div>
        </div>
    )
}


export default SectionTwoRoles