import React from 'react';
// import userProfile from '../../../public/assets/user/user_profile.png';

const SectionOne = (props) => {
    const {assists, averageKDA, wins, winrate, played, losses, kills, deaths} = props.dataSummary
    const userProfile = props.profileIcon ? props.profileIcon : null
    const progressBarWidth = param => {
        return (
            {
                width: `calc(0% + ${param}%)`
            }
        )
    }
    return (
        <div className="section_one">
            <div className="profile_pic_wrapper"><img src={userProfile} alt="profile" className="profile_pic" />
                <span className="profile_level font_bebas">{`Level ${props.summonerLevel ? props.summonerLevel : null}`}</span>
            </div>
            <div className="summoner_wrapper">
                <div className="user_label">Summoner Name</div>
                <div className="font_bebas summoner_name">{props.summonerName ? props.summonerName: null}</div>
                <div className="user_label">Location</div>
                <div className="location_name font_bebas">{props.region ? props.region : null}</div>
            </div>
            <div className="player_stats_wrapper">
                <div className="user_label border_underline user_subtitle">
                    Player Stats
            </div>
                <div className="player_stats_bottom">
                    <div className="psb__left">
                        <div className="user_label">Average KDA</div>
                        <div className="user_val font_bebas">
                            {averageKDA ? averageKDA.toFixed(2) : null}
                        </div>
                        <span className="red_progress_bar">
                            <span className="green_progress_bar" style={progressBarWidth(35)}></span>
                        </span>
                    </div>
                    <span className="vertical_border"></span>
                    <div className="psb__right">
                        <div className="user_label">Win Rate</div>
                        <div className="user_val font_bebas">
                            {winrate ? `${winrate.toFixed(2)}%`: null}
                        </div>
                        <span className="red_progress_bar">
                            <span className="green_progress_bar" style={progressBarWidth(winrate.toFixed(2))}></span>
                        </span>
                    </div>
                </div>
            </div>
            <div className="recent_stats_wrapper">
                <div className="user_label border_underline user_subtitle">
                    Recent Match Stats
            </div>
                <div className="player_stats_bottom">
                    <div className="psb__left">
                        <div className="psb_played">
                            <div className="user_label">Played</div>
                            <span className="user_val font_bebas">{played ? played : null}</span>
                        </div>
                        <div className="psb_wins">
                            <div className="user_label">Wins</div>
                            <span className="user_val font_bebas">{wins ? wins : null}</span>
                        </div>
                        <div className="psb_loss">
                            <div className="user_label">Loss</div>
                            <span className="user_val font_bebas">{losses ? losses : null}</span>
                        </div>
                    </div>
                    <span className="vertical_border"></span>
                    <div className="psb__right">
                        <div className="psb_kills">
                            <div className="user_label">Kills</div>
                            <span className="user_val font_bebas">{kills ? kills : null}</span>
                        </div>
                        <div className="psb_deaths">
                            <div className="user_label">Deaths</div>
                            <span className="user_val font_bebas">{deaths ? deaths : null}</span>
                        </div>
                        <div className="psb_loss">
                            <div className="user_label">Assists</div>
                            <span className="user_val font_bebas">{assists ? assists : null}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}


export default SectionOne