import React from 'react';
import ChartBlock from './ChartBlock';
// import champ1 from '../../../public/assets/user/champ_1.png'
// import champ2 from '../../../public/assets/user/champ_2.png'
// import champ3 from '../../../public/assets/user/champ_3.png'
// import champ4 from '../../../public/assets/user/champ_4.png'
// import champ5 from '../../../public/assets/user/champ_5.png'



const SectionThreeChampions = (props) => {

    return (
        <div className="section_two">
            <div className="st__top_row">
                <div className="font_bebas sub_title">{props.subTitle ? props.subTitle : null}</div>
                <div className="color_block_wrapper">
                    <div className="cb_holder">
                        <span className="cb cb__gray"></span>
                        <span className="inline_block">Games</span>
                    </div>
                    <div className="cb_holder">
                        <span className="cb cb__green"></span>
                        <span className="inline_block">Wins</span>
                    </div>
                    <div className="cb_holder">
                        <span className="cb cb__red"></span>
                        <span className="inline_block">Losses</span>
                    </div>
                </div>
            </div>
            <div className="st__bottom_row">
                {
                    props.champions ?
                        Object.values(props.champions).slice(0,5).map((arr, key) => {
                            return (
                                <ChartBlock key={key} userPhoto={arr.icon} smallUserPhoto={false} games={arr.games} losses={arr.losses} wins={arr.wins} />
                            )
                        })
                        : <p className="no_data_msg font_size_twice">NA</p>
                }
            </div>
        </div>
    )
}


export default SectionThreeChampions