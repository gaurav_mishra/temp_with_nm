import React from 'react';
import ColumnBlock from './Columnblock';
import { timeConvert, formattedDate } from '../../util/utils';
import moment from 'moment';

const GameRow = (props) => {
    const userIcon = props.matchData.championIcon ? props.matchData.championIcon : null
    const kills = typeof (props.matchData.kills) === "number" ? props.matchData.kills : 'NA'
    const deaths = typeof (props.matchData.deaths) === "number" ? props.matchData.deaths : 'NA'
    const assists = typeof (props.matchData.assists) === "number" ? props.matchData.assists : 'NA'
    const kda = `${kills}/${deaths}/${assists}`
    const status = props.matchData.status ? props.matchData.status : null
    const items = props.matchData.items ? props.matchData.items : null
    const playedAt = props.matchData.playedAt ? props.matchData.playedAt : null
    const matchDuration = props.matchData.timeDuration ? props.matchData.timeDuration : null
    const timeDuration = timeConvert(matchDuration)
    const time = moment(playedAt);
    const roleName = props.matchData.role ? props.matchData.role : null
    const championName = props.matchData.championName ? props.matchData.championName : null


    var startDate = moment(time, "DD.MM.YYYY");
    var endDate = moment(formattedDate, "DD.MM.YYYY");

    var playedDaysAgo = endDate.diff(startDate, 'days');



    return (
        <div className="game_row_wrapper">
            <div className="gr__topbar">
                <div className="font_bebas gr__text">
                    {roleName}
                </div>
                <div className="gr__subtext">
                    {!isNaN(playedDaysAgo)?
                        (`${playedDaysAgo} days ago`
                        )
                        :
                        null
                    }
                </div>
            </div>
            <div className="gr__container">
                <div className="gr__row">
                    <div className="game_pp_wrapper">
                        <img src={userIcon} alt="user" className="game_pp" />
                        <div className="font_bebas game_usrname">{championName}</div>
                    </div>
                    <ColumnBlock customClass={'fixed_width_360'} label={'Items'} icons={items} />
                    <ColumnBlock customClass={'fixed_width'} icons={null} label={'Kills'} value={kills} />
                    <ColumnBlock customClass={'fixed_width'} icons={null} label={'Deaths'} value={deaths} />
                    <ColumnBlock customClass={'fixed_width'} icons={null} label={'Assits'} value={assists} />
                    <ColumnBlock customClass={'fixed_width'} icons={null} label={'KDA'} kda={kda} />
                    <ColumnBlock customClass={'fixed_width'} icons={null} label={'Time'} value={timeDuration} />
                    <ColumnBlock customClass={'fixed_width'} icons={null} label={'Status'} status={status} />
                </div>
            </div>
        </div>
    )
}


export default GameRow