import React from "react";
import "../styles/side-drawer.css";
import close from "../../public/assets/common-icons/close.svg";
import appStore from "../../public/assets/common-icons/app_store.svg";
import googlePlay from "../../public/assets/common-icons/google_play.svg";
// import { hideSideBar } from "../actions/actionDispatcher";
// import store from "./../store.js";

const SideDrawer = props => {
  // setTimeout(() => {
  //   store.dispatch(hideSideBar());
  // }, 15000);
  return (
    <aside className={`side_drawer_wrapper ${props.showFlag ? `reveal` : ``}`}>
      <div className="login_wrapper">
        <div className="uport_title">
          <span>LOGIN</span> VIA UPORT
        </div>
        <div>
          <p>
            Open the Uport app and scan the barcode to login to the applcaiton.
          </p>
          <div>
            <p className="uport_pointers">1.Open the uport app</p>
            <p className="uport_pointers">2. Open the QR Code scanner </p>
            <p className="uport_pointers">3. Scan the qr code</p>
          </div>
        </div>
      </div>
      <div className="register_wrapper">
        <div className="uport_title">
          <span>REGISTER</span> ON UPORT
        </div>
        <div>
          <p>
            Download the uPort app to your smartphone and follow the
            instructions of the app.
          </p>
          <p>
            The uPort app is your digital data safe for personal information.
            You alone determine who can access the data and when.
          </p>
          <div>
            <p className="uport_pointers">1.Open the uport app</p>
            <p className="uport_pointers">2. Open the QR Code scanner </p>
            <p className="uport_pointers">3. Scan the qr code</p>
          </div>
        </div>
      </div>
      <div className="icons_wrapper">
        <a className="store_icon" target="_blank" href="#">
          <img className="" alt="" src={appStore} />
        </a>
        <a className="store_icon" target="_blank" href="#">
          <img className="" alt="" src={googlePlay} />
        </a>
      </div>
      <a
        className="close_button"
        onClick={event => props.oncloseSideDrawerClick(event)}
      >
        <img className="" alt="close" src={close} />
      </a>
    </aside>
  );
};

export default SideDrawer;
