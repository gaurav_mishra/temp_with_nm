import React from "react";
// import close from "../../public/assets/common-icons/close.svg";

const Modal = (props) => {
  return (
    <div>
      <div className="modal_overlay">
        <div className="modal_component">
          <div className="modal_header">
            {/* <a
              className="close_button"
            >
              <img className="" alt="close" src={close} />
            </a> */}
          </div>
          <div className="modal_body">
            {props.children}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Modal;
