import React from "react";
import SectionOne from "./userProfileSections/SectionOne";
import SectionTwoChampions from "./userProfileSections/SectionTwoChampions";
import SectionThree from "./userProfileSections/SectionThree";
import SectionTwoRoles from "./userProfileSections/SectionTwoRoles";



const UserProfile = (props) => {
    const {assists, averageKDA, wins, winrate, played, losses, kills, deaths, summonerLevel, profileIcon} = props.userProfileData
    const {roles, champions, matches} = props.userProfileData
    const {summonerName, region} = props
    const matchesLength = props.matches ? props.matches.length : null

    const dataSummary = { assists, averageKDA, wins, winrate, played, losses, kills, deaths }
    
   
    return (
        <div className="user_profile_wrapper">
            <SectionOne  summonerName={summonerName} region={region} summonerLevel={summonerLevel} profileIcon={profileIcon} dataSummary={dataSummary} />
            <div className="divider_wrapper">
                <SectionTwoRoles matchesLength={matchesLength} roles={roles} subTitle={"Recently Played Roles"}/>
                <SectionTwoChampions matchesLength={matchesLength} subTitle={"Top 5 played Champions"} champions={champions} />
            </div>
            <SectionThree matches={matches}/>
        </div>
    )
}



export default UserProfile