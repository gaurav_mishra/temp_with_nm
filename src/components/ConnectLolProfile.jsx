import React from "react";
import Modal from "../components/Modal";
import dropArrow from "../../public/assets/common-icons/down_arrow.png";
import Constants from "../../src/Constants"


const ConnectLolProfile = (props) => {
    const locationList = Constants.locationList
    return (
        <Modal>
            <div className="font_bebas connect_title">Welcome! <span className="text_pink">{props.body.firstName}</span></div>
            <div className="connect_bottom">
                <div className="connect_desc">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quis at sit dui eu euismod mauris ut mauris,
                    bibendum. Posuere suscipit sit id elementum. Neque, in est aliquet aenean pulvinar at. Non nunc elit
                    eu aliquam.
                </div>
            </div>
            <div className="connect_bottom">
                <div className="font_bebas connect_sub_title">Verify Personal Details</div>
                <div className="connect_details">
                    <div className="connect_txt_wrapper">
                        <div className="txt_placeholder">
                            Name
                        </div>
                        <div className="txt_value">
                            {props.body.firstName}
                        </div>
                    </div>
                    {props.body.mobile ?
                        <div className="connect_txt_wrapper">
                            <div className="txt_placeholder">
                                Mobile Number
                        </div>
                            <div className="txt_value">
                                {props.body.mobile}
                            </div>
                        </div>
                        : null}
                    {props.body.email ?
                        <div className="connect_txt_wrapper">
                            <div className="txt_placeholder">
                                Email Address
                        </div>
                            <div className="txt_value">
                                {props.body.email}
                            </div>
                        </div>
                        : null}
                </div>
            </div>
            <form className="summoner_form" id="summonerName">
                <div className="connect_bottom">
                    <div className="font_bebas connect_sub_title">Connect your league of legends account</div>
                    <div className="connect_details">
                        <div className="connect_txt_wrapper">
                            <div className="txt_placeholder">
                                Summoner Name
                            </div>
                            <div className="txt_input connect_input">
                                <input type="text" placeholder="Enter summoner name" className="" onChange={(e) => props.updateSummonerName(e)} />
                            </div>
                        </div>
                        <div className="connect_txt_wrapper">
                            <div className="txt_placeholder">
                                Location
                            </div>
                            <div className="txt_input dropdown_location_input">
                                <div className="loc_visible_value" onClick={props.handleLocation}>{props.location}<img className="drop_arrow" src={dropArrow} alt="down arrow" /></div>
                                {props.showLocation ?
                                    <ul className="loc_list">
                                        {locationList.map((listItem, key) =>
                                            <li className="list_item" key={key} onClick={() => props.setLocation(listItem)}>{listItem}</li>
                                        )}
                                    </ul>
                                    :
                                    null}

                            </div>
                        </div>
                    </div>
                </div>
                <div className="connect_btn_wrapper">
                    <a className="btn-submit connect_btn_submit font_bebas" type="submit" onClick={props.formSubmit}>Continue</a>
                </div>
                {
                    props.formError ?
                        <div className="connect_error">Please fill the fields</div>
                        : null
                }
                {
                    props.credentialsError ?
                        <div className="connect_error">Details could not be fetched with the entered values, kindly try again!</div>
                        : null
                }
            </form>
        </Modal >
    )
}



export default ConnectLolProfile