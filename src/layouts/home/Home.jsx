import React from "react";
import lolChar from "../../../public/assets/home_lol_char.svg";

const Home = () => {
  return (
    <main className="container">
      <div className="home_img_wrapper">
        <img className="lol_char" alt="league of legends" src={lolChar} />
      </div>
      <div className="home_content_wrapper">
        <h1 className="home_content_title">Blockchain - Gaming Identity</h1>
        <p className="home_content_para">
          Gamers are represented as Ethereum addresses, but behind those addresses are actual people with interactions and gaming reputation that forms a digital identity when considered together. A gamers identity is dynamic and constantly changing based on the games played, wins, losses, etc. An identity is not just based on what others say about them, but also on the things they do. Their interactions with other gamers, businesses, and blockchains all form parts of who they are and how they choose to represent themselves for future interactions. Here at NextGenTM, we have built a platform where users can create their identity and portfolio
          </p>
      </div>
    </main>
  );
}

export default Home;

