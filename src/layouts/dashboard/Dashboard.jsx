import React, { Component } from 'react'
import { postUserDetails, getUserDetails, header } from '../../apis/apis'
import axios from 'axios'
import store from '../../store.js'
import { addPlayerID, showLoaderOnConnectFormSubmit, addUserDetails } from '../../actions/actionDispatcher'
import { connect } from 'react-redux'
import ConnectLolProfile from "../../components/ConnectLolProfile";
import UserProfile from "../../components/UserProfile";
import { defaultRegionValue } from '../../Constants';


const mapStateToProps = state => {
  return {
    userProfileData: state.app,
  }
}

const mapDispatchToProps = dispatch => {
  return {

  }
}
class Dashboard extends Component {
  constructor(props, { authData, userProfileData }) {
    super(props)
    authData = this.props
    userProfileData = this.props
    this.state = {
      isUserDetails: false,
      isLocationDropdown: false,
      isConnectLolError: false,
      currentLocation: defaultRegionValue,
      summonerName: '',
      userFormData: {},
      resSummonerName: null,
      resRegion: null,
      resPlayerID: null,
      isUserCredentialsValid: false,
      userProfileData: {},
      isConnectLolProfile: false,
    }
  }


  componentDidMount() {
    this.sendUserInfo()
  }


  handleLocationDropdown = () => {
    this.setState({
      isLocationDropdown: !this.state.isLocationDropdown
    })
  }

  handleSetLocation = curr => {
    this.setState({
      currentLocation: curr
    })
    this.handleLocationDropdown()
  }

  handleSummonerName = e => {
    this.setState({
      summonerName: e.target.value
    })
  }

  handleFormSubmit = () => {
    const summonerName = this.state.summonerName
    const tempLocation = this.state.currentLocation
    if (summonerName && tempLocation !== 'Select') {
      this.setState({
        isConnectLolError: false,
        isConnectLolProfile: false,
      })
      store.dispatch(showLoaderOnConnectFormSubmit())
      const location = tempLocation.toLowerCase();
      this.getUserDetails(summonerName, location);
    }
    else {
      this.setState({
        isConnectLolError: true,
        isUserCredentialsValid: false,
      })
    }
  }



  sendUserInfo = () => {
    const url = postUserDetails;
    const { authData } = this.props

    let firstName = authData.name && authData.name
    let email = authData.email && authData.email
    let dob = authData.dob && authData.dob
    let mobileNumber = authData.phone && authData.phone
    let blockchainId = authData.ETHaddress && authData.ETHaddress
    const body = {}

    if (firstName) body.firstName = firstName
    if (email) body.email = email
    if (dob) body.dob = dob
    if (mobileNumber) body.mobileNumber = mobileNumber
    if (blockchainId) body.blockchainId = blockchainId

    this.setState({
      userFormData: body
    })

    axios.post(url, body, { header: header })
      .then(res => {
        const response = res.data.data;
        const playerID = response.PlayerID;
        const resSummonerName = response.SummonerName;
        const resRegion = response.PlatformID;

        this.setState({
          resPlayerID: playerID,
        })

        if (resSummonerName && resRegion) {
          this.setState({
            resSummonerName: resSummonerName,
            resRegion: resRegion,
            isConnectLolProfile: false,
            
          })
          this.getUserDetails();
        }
        else{
          this.setState({
            isConnectLolProfile: true
          })
        }
        store.dispatch(addPlayerID(playerID));
      })
      .catch(err => {
        console.log('failure');
        console.log(err)
      })
  }

  getUserDetails = (summonerName = this.state.resSummonerName, region = this.state.resRegion) => {
    let playerID = this.state.resPlayerID;
    let tempUrl = getUserDetails;
    let url = `${tempUrl}${summonerName}/${region}/${playerID}`;
    axios.get(url, { header: header })
      .then(res => {
        const response = res.data.data;
        store.dispatch(addUserDetails(response));
        this.setState({
          isUserCredentialsValid: false,
          isUserDetails: true,
          userProfileData: response
        })
      })
      .catch(err => {
        console.log(err);
        this.setState({
          isUserCredentialsValid: true,
          location: defaultRegionValue,
          summonerName: ''
        })
      })
    
  }

  render() {
    const { authData } = this.props
    const { isUserDetails, isLocationDropdown, summonerName, currentLocation, isConnectLolError, userFormData,isConnectLolProfile, isUserCredentialsValid, userProfileData, resSummonerName, resRegion } = this.state
    const fullHeight = {
      'height': '78vh'
    }
    return (
      <main className="container" style={isUserDetails === false ? fullHeight : null}>
        {this.props.userProfileData.showLoaderOnConnectFormSubmit ? <div className="loader_on_user_profile"><p>Please Wait...</p></div> :
          isUserDetails ?
            // ""
            userProfileData && (resSummonerName || summonerName) && (resRegion || currentLocation) && !isConnectLolProfile &&
            <UserProfile userProfileData={userProfileData} summonerName={resSummonerName || summonerName} region={resRegion || currentLocation} />
            :
            (isConnectLolProfile &&
            <ConnectLolProfile
              showLocation={isLocationDropdown}
              handleLocation={this.handleLocationDropdown}
              setLocation={this.handleSetLocation}
              location={currentLocation}
              updateSummonerName={this.handleSummonerName}
              formSubmit={this.handleFormSubmit}
              formError={isConnectLolError}
              body={userFormData}
              credentialsError={isUserCredentialsValid}
            />)
        }
      </main>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard)
