import React from "react";

import discordIcon from '../../../public/assets/common-icons/discord.svg'
import facebookIcon from '../../../public/assets/common-icons/facebook.svg'
import mediumIcon from '../../../public/assets/common-icons/medium.svg'
import telegramIcon from '../../../public/assets/common-icons/telegram.svg'
import twitterIcon from '../../../public/assets/common-icons/twitter.svg'
import wechatIcon from '../../../public/assets/common-icons/wechat.svg'


const Footer = () => {
  return (
    <footer className="footer">
      <div className="footer_content_wrapper">
        <div className="copywright_container">
          <p>Copyright LOL &copy;2020</p>
        </div>
        <div className="social_icons_container">
          <a className="footer_social_icon" href="" target="_blank">
            <img className="" src={discordIcon} alt="discord icon" />
          </a>
          <a className="footer_social_icon" href="" target="_blank">
            <img className="" src={facebookIcon} alt="facebok icon" />
          </a>
          <a className="footer_social_icon" href="" target="_blank">
            <img className="" src={mediumIcon} alt="medium icon" />
          </a>
          <a className="footer_social_icon" href="" target="_blank">
            <img className="" src={telegramIcon} alt="telegram icon" />
          </a>
          <a className="footer_social_icon" href="" target="_blank">
            <img className="" src={twitterIcon} alt="twitter icon" />
          </a>
          <a className="footer_social_icon" href="" target="_blank">
            <img className="" src={wechatIcon} alt="wechat icon" />
          </a>
        </div>
        <div className="powered_container">
          <p className="powered_text">
            Powered by <span className="text_underline">Voice of Gamers</span>
          </p>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
