import Actions from "../actions/actions";
const initState = {
  showSideBar: false,
  showLoaderOnConnectFormSubmit: true,
  playerID: null,
  playerData: {},
};

const appReducer = (state = initState, action) => {
  switch (action.type) {
    case Actions.SHOW_SIDEBAR:
      return {
        ...state,
        showSideBar: true
      };
    case Actions.HIDE_SIDEBAR:
      return {
        ...state,
        showSideBar: false
      };
    case Actions.ADD_PLAYER_ID:
      return {
        ...state,
        playerID: action.payload,
        showLoaderOnConnectFormSubmit: false,
      };
      case Actions.SHOW_LOADER_ON_CONNECT_FORM_SUBMIT:
      return {
        ...state,
        showLoaderOnConnectFormSubmit: true
      };
      case Actions.SAVE_USER_DATA:
        return{
          ...state,
          playerData: action.payload,
          showLoaderOnConnectFormSubmit: false,
        }
    default:
      return state;
  }
};

export default appReducer;
